package net.tardis.mod.potions;

import net.minecraft.entity.LivingEntity;
import net.minecraft.potion.Effect;
import net.minecraft.potion.EffectType;
import net.tardis.mod.damagesources.TDamageSources;

public class MercuryEffect extends Effect {

	public MercuryEffect(int liquidColorIn) {
		super(EffectType.HARMFUL, liquidColorIn);
	}

	@Override
	public void performEffect(LivingEntity entityLivingBaseIn, int amplifier) {
		super.performEffect(entityLivingBaseIn, amplifier);
		if(entityLivingBaseIn != null && entityLivingBaseIn.ticksExisted % 20 == 0)
			entityLivingBaseIn.attackEntityFrom(TDamageSources.MERCURY, 1.0F);
	}

	@Override
	public boolean isReady(int duration, int amplifier) {
		return true;
	}
}
