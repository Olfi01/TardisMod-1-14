package net.tardis.mod.tileentities;

import java.util.Set;

import net.minecraft.util.math.ChunkPos;

public interface IChunkLoader {
	
	ChunkLoaderTile<?> getChunkLoader();
	
	Set<ChunkPos> getChunkSet();

}
