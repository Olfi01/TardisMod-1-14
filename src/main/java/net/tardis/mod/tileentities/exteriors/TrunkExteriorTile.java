package net.tardis.mod.tileentities.exteriors;

import net.minecraft.state.properties.BlockStateProperties;
import net.minecraft.tileentity.TileEntityType;
import net.minecraft.util.math.AxisAlignedBB;
import net.tardis.mod.texturevariants.TextureVariants;
import net.tardis.mod.tileentities.TTiles;

public class TrunkExteriorTile extends ExteriorTile{

    public static final AxisAlignedBB RENDER = new AxisAlignedBB(-1, -2, -1, 2, 2, 2);
	
    public static final AxisAlignedBB NORTH_BOX = new AxisAlignedBB(0, -1, -0.1, 1, 1, 1);
	public static final AxisAlignedBB EAST_BOX = new AxisAlignedBB(0, -1, 0, 1.1, 1, 1);
	public static final AxisAlignedBB SOUTH_BOX = new AxisAlignedBB(0, -1, 0, 1, 1, 1.1);
	public static final AxisAlignedBB WEST_BOX = new AxisAlignedBB(-0.1, -1, 0, 1, 1, 1);
    
    
	public TrunkExteriorTile(TileEntityType<?> tileEntityTypeIn) {
		super(tileEntityTypeIn);
		this.setVariants(TextureVariants.TRUNK);
	}
	
	public TrunkExteriorTile() {
		this(TTiles.EXTERIOR_TRUNK.get());
	}

	@Override
	public AxisAlignedBB getDoorAABB() {
		if(world != null && world.getBlockState(getPos()).hasProperty(BlockStateProperties.HORIZONTAL_FACING)) {
			switch(world.getBlockState(getPos()).get(BlockStateProperties.HORIZONTAL_FACING)) {
			case EAST: return EAST_BOX;
			case SOUTH: return SOUTH_BOX;
			case WEST: return WEST_BOX;
			default: return NORTH_BOX;
			}
		}
		return NORTH_BOX;
	}

    @Override
    public AxisAlignedBB getRenderBoundingBox() {
        return RENDER.offset(getPos());
    }

}
