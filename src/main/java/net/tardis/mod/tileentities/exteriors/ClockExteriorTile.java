package net.tardis.mod.tileentities.exteriors;

import net.minecraft.util.SoundCategory;
import net.minecraft.util.math.AxisAlignedBB;
import net.tardis.mod.enums.EnumMatterState;
import net.tardis.mod.tileentities.TTiles;
import net.tardis.mod.sounds.TSounds;

public class ClockExteriorTile extends ExteriorTile{
	
	private AxisAlignedBB renderBox;
	
	public ClockExteriorTile() {
		super(TTiles.EXTERIOR_CLOCK.get());
	}

	@Override
	public AxisAlignedBB getDoorAABB() {
		return this.getDefaultEntryBox();
	}

    @Override
    public AxisAlignedBB getRenderBoundingBox() {
        return this.renderBox != null ? this.renderBox : (renderBox = new AxisAlignedBB(this.getPos()).grow(3));
    }

	@Override
	public void tick() {
		super.tick();
		if(!world.isRemote && world.getGameTime() % 60 == 0) {
			world.playSound(null, this.getPos(), this.getMatterState() != EnumMatterState.SOLID ? TSounds.WATCH_MALFUNCTION.get() : TSounds.WATCH_TICK.get(), SoundCategory.BLOCKS, 1F, 1F);
		}
	}


}
