package net.tardis.mod.tileentities.inventory;

import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.inventory.IInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.nbt.INBT;
import net.minecraft.nbt.ListNBT;
import net.minecraft.util.NonNullList;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraftforge.common.util.INBTSerializable;

public class BatteryInventory implements IInventory, INBTSerializable<ListNBT>{

	private NonNullList<ItemStack> inv = NonNullList.withSize(3, ItemStack.EMPTY);
	
	public BatteryInventory() {
	}
	
	@Override
	public void clear() {
		inv.clear();
		this.markDirty();
	}

	@Override
	public int getSizeInventory() {
		return inv.size();
	}

	@Override
	public boolean isEmpty() {
		return inv.isEmpty();
	}

	@Override
	public ItemStack getStackInSlot(int index) {
		return index < inv.size() ? inv.get(index) : ItemStack.EMPTY;
	}

	@Override
	public ItemStack decrStackSize(int index, int count) {
		this.markDirty();
		return this.getStackInSlot(index).split(count);
	}

	@Override
	public ItemStack removeStackFromSlot(int index) {
		ItemStack old = this.getStackInSlot(index);
		this.setInventorySlotContents(index, ItemStack.EMPTY);
		this.markDirty();
		return old;
	}

	@Override
	public void setInventorySlotContents(int index, ItemStack stack) {
		if(index < inv.size())
			inv.set(index, stack);
		this.markDirty();
	}

	@Override
	public void markDirty() {
		
	}

	@Override
	public boolean isUsableByPlayer(PlayerEntity player) {
		return true;
	}

	@Override
	public ListNBT serializeNBT() {
		ListNBT tag = new ListNBT();
		for(ItemStack stack : inv) {
			tag.add(stack.serializeNBT());
		}
		return tag;
	}

	@Override
	public void deserializeNBT(ListNBT nbt) {
		int index = 0;
		for(INBT tag : nbt) {
			this.inv.set(index, (ItemStack.read((CompoundNBT)tag)));
			++index;
		}
	}

	@Override
	public void openInventory(PlayerEntity player) {}

	@Override
	public void closeInventory(PlayerEntity player) {}

	public ITextComponent getName() {
		return new TranslationTextComponent("vm.inventory.battery");
	}
}
