package net.tardis.mod.boti;

import net.minecraft.network.PacketBuffer;
import net.minecraft.util.math.BlockPos;
import net.tardis.mod.boti.stores.BlockStore;

import java.util.HashMap;
import java.util.Map;

public class BotiChunk {

    private HashMap<BlockPos, BlockStore> map = new HashMap<>();

    public BotiChunk(HashMap<BlockPos, BlockStore> map){
        this.map.putAll(map);
    }

    public BotiChunk(){}

    public BotiChunk(PacketBuffer buf){
        this.decode(buf);
    }

    public void addToChunk(BlockPos pos, BlockStore store){
        this.map.put(pos, store);
    }

    public HashMap<BlockPos, BlockStore> getMap(){
        return this.map;
    }

    public void encode(PacketBuffer buf){
        buf.writeInt(this.map.size());

        for(Map.Entry<BlockPos, BlockStore> entry : map.entrySet()){
            buf.writeBlockPos(entry.getKey());
            entry.getValue().encode(buf);
        }

    }
    public void decode(PacketBuffer buf){
        int size = buf.readInt();

        for(int i = 0; i < size; ++i){
            this.addToChunk(buf.readBlockPos(), new BlockStore(buf));
        }

    }

}
