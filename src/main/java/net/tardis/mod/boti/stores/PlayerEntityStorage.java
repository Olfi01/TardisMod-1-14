package net.tardis.mod.boti.stores;

import com.mojang.authlib.GameProfile;
import net.minecraft.client.Minecraft;
import net.minecraft.client.entity.player.AbstractClientPlayerEntity;
import net.minecraft.client.entity.player.ClientPlayerEntity;
import net.minecraft.client.world.ClientWorld;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.network.PacketBuffer;
import net.minecraft.world.World;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;
import net.tardis.mod.boti.BotiPlayer;
import net.tardis.mod.boti.BotiWorld;

public class PlayerEntityStorage extends EntityStorage {

    public PlayerEntityStorage(PlayerEntity e) {
        super(e);
    }

    public PlayerEntityStorage(PacketBuffer buffer){
        super(buffer);
    }

    @Override
    public EntityStorageTypes getStoreType() {
        return EntityStorageTypes.PLAYER;
    }

    @OnlyIn(Dist.CLIENT)
    @Override
    public Entity create(World world) {

        this.entity = new BotiPlayer((ClientWorld) world, new GameProfile(this.id, null));
        entity.deserializeNBT(this.data);
        return entity;
    }
}
