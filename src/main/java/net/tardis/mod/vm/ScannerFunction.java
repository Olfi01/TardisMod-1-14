package net.tardis.mod.vm;


import java.util.List;

import net.minecraft.entity.Entity;
import net.minecraft.entity.monster.MonsterEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraft.world.World;
import net.minecraft.world.server.ServerWorld;
import net.tardis.mod.client.ClientHelper;
import net.tardis.mod.constants.TardisConstants;
import net.tardis.mod.helper.PlayerHelper;
import net.tardis.mod.misc.ObjectWrapper;


/**
 * Created by 50ap5ud5
 * on 5 May 2020 @ 8:04:17 pm
 */
public class ScannerFunction extends BaseVortexMFunction {

    @Override
    public void sendActionOnClient(World world, PlayerEntity player) {
        ClientHelper.openGUI(TardisConstants.Gui.NONE, null);
        PlayerHelper.closeVMModel(player);
    }

    @Override
    public void sendActionOnServer(World world, ServerPlayerEntity player) {
        ObjectWrapper<Integer> wrapPlayer = new ObjectWrapper<>(0);
        ObjectWrapper<Integer> warpMonster = new ObjectWrapper<>(0);
        ServerWorld server = (ServerWorld) world;
        int radius = 30;
        AxisAlignedBB scanRange = new AxisAlignedBB(player.getPosition()).grow(radius);
        List<Entity> list = server.getEntitiesWithinAABB(Entity.class, scanRange);
        list.forEach(entity -> {
            if (entity instanceof PlayerEntity && entity != player) {
                wrapPlayer.setValue(wrapPlayer.getValue() + 1);
            }
            if (entity instanceof MonsterEntity) {
                warpMonster.setValue(warpMonster.getValue() + 1);
            }
        });

        PlayerHelper.sendMessageToPlayer(player, new TranslationTextComponent("message.vm.scanner.result", wrapPlayer.getValue(), warpMonster.getValue(), radius), false);
        super.sendActionOnServer(world, player);
    }

    @Override
    public VortexMUseType getLogicalSide() {
        return VortexMUseType.BOTH;
    }


}
