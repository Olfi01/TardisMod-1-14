package net.tardis.mod.network;

import net.minecraft.network.PacketBuffer;
import net.minecraft.util.ResourceLocation;

import javax.annotation.Nullable;

public class NetworkHelper {

    public static void writeOptionalRL(PacketBuffer buf, @Nullable ResourceLocation resourceLocation){
        buf.writeBoolean(resourceLocation != null);
        if(resourceLocation != null)
            buf.writeResourceLocation(resourceLocation);
    }

    @Nullable
    public static ResourceLocation readOptionalRL(PacketBuffer buf){
        boolean exists = buf.readBoolean();
        if(exists)
            return buf.readResourceLocation();
        return null;
    }

}
