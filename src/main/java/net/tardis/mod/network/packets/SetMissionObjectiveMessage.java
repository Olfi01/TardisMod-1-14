package net.tardis.mod.network.packets;

import java.util.function.Supplier;

import net.minecraft.network.PacketBuffer;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.BlockPos;
import net.minecraftforge.fml.network.NetworkEvent;
import net.tardis.mod.cap.Capabilities;
import net.tardis.mod.missions.MiniMission;

public class SetMissionObjectiveMessage {

	private BlockPos pos;
	private int objective;
	private ResourceLocation stage;
	
	public SetMissionObjectiveMessage(BlockPos pos, int objective, ResourceLocation stage) {
		this.pos = pos;
		this.objective = objective;
		this.stage = stage;
	}
	
	public static void encode(SetMissionObjectiveMessage mes, PacketBuffer buffer) {
		buffer.writeBlockPos(mes.pos);
		buffer.writeInt(mes.objective);
		buffer.writeResourceLocation(mes.stage);
	}
	
	public static SetMissionObjectiveMessage decode(PacketBuffer buf) {
		return new SetMissionObjectiveMessage(buf.readBlockPos(), buf.readInt(), buf.readResourceLocation());
	}
	
	public static void handle(SetMissionObjectiveMessage mes, Supplier<NetworkEvent.Context> cont) {
		
		cont.get().enqueueWork(() -> {
			cont.get().getSender().getServerWorld().getCapability(Capabilities.MISSION).ifPresent(mission -> {
				MiniMission mis = mission.getMissionForPos(mes.pos);
				if(mis != null) {
					mis.setObjectiveForStage(mes.stage, mes.objective);
					
				}
			});
		});
		
		cont.get().setPacketHandled(true);
	}
}
