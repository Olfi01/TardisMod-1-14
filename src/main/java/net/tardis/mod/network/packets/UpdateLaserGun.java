package net.tardis.mod.network.packets;

import java.util.function.Supplier;

import net.minecraft.item.ItemStack;
import net.minecraft.network.PacketBuffer;
import net.minecraft.util.Hand;
import net.minecraftforge.fml.network.NetworkEvent;
import net.tardis.mod.helper.PlayerHelper;
import net.tardis.mod.items.LaserGunItem;

public class UpdateLaserGun {
    
    private ItemStack stack;
    
    public UpdateLaserGun(ItemStack stack) {
        this.stack = stack;
    }
    
    public static void encode(UpdateLaserGun mes, PacketBuffer buf) {
        buf.writeItemStack(mes.stack);
    }
    
    public static UpdateLaserGun decode(PacketBuffer buf) {
        return new UpdateLaserGun(buf.readItemStack());
    }
    
    public static void handle(UpdateLaserGun mes, Supplier<NetworkEvent.Context> context) {
        context.get().enqueueWork(() -> {
            ItemStack stack = context.get().getSender().getHeldItem(PlayerHelper.isInMainHand(context.get().getSender(), mes.stack.getItem()) ? Hand.MAIN_HAND : Hand.OFF_HAND);
            if (stack.getItem() instanceof LaserGunItem) {
                LaserGunItem gun = (LaserGunItem)stack.getItem();
                ItemStack ammo = gun.getBatteryInInventory(context.get().getSender().inventory.mainInventory);
                gun.addBattery(context.get().getSender(), stack, ammo);
            }
        });
        context.get().setPacketHandled(true);
    }

}
