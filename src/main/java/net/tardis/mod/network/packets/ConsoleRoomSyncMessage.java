package net.tardis.mod.network.packets;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Supplier;

import com.mojang.serialization.Codec;

import net.minecraft.nbt.CompoundNBT;
import net.minecraft.nbt.NBTDynamicOps;
import net.minecraft.network.PacketBuffer;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fml.network.NetworkEvent;
import net.tardis.mod.ars.ConsoleRoom;

public class ConsoleRoomSyncMessage {
    
	private Map<ResourceLocation, ConsoleRoom> consoleRooms = new HashMap<ResourceLocation, ConsoleRoom>();
	//We use an unboundedMapCodec. However it is limited in that it can only parse objects whose keys can be serialised to a string, such as ResourceLocation
	//E.g. If you used an int as a key, the unboundedMapCodec will not parse it and will error.
	public static final Codec<Map<ResourceLocation, ConsoleRoom>> MAPPER = Codec.unboundedMap(ResourceLocation.CODEC, ConsoleRoom.getCodec());
	
	
	public ConsoleRoomSyncMessage(Map<ResourceLocation, ConsoleRoom> consoleRooms) {
		this.consoleRooms.clear(); //Clear the client's list and readd the entries from the server
        this.consoleRooms.putAll(consoleRooms);
	}

	public static void encode(ConsoleRoomSyncMessage mes, PacketBuffer buf) {
		buf.writeCompoundTag((CompoundNBT)(MAPPER.encodeStart(NBTDynamicOps.INSTANCE, mes.consoleRooms).result().orElse(new CompoundNBT())));
	}
	
	public static ConsoleRoomSyncMessage decode(PacketBuffer buf) {
		//Parse our Map Codec and send the nbt data over. If there's any errors, populate with default Tardis Mod console rooms
		return new ConsoleRoomSyncMessage(MAPPER.parse(NBTDynamicOps.INSTANCE, buf.readCompoundTag()).result().orElse(ConsoleRoom.registerCoreConsoleRooms()));
	}
	
	public static void handle(ConsoleRoomSyncMessage mes, Supplier<NetworkEvent.Context> cont) {
		cont.get().enqueueWork(() -> {
			ConsoleRoom.DATA_LOADER.setData(mes.consoleRooms); //Set the ConsoleRoom's Registry to that of the parsed in list
		});
		cont.get().setPacketHandled(true);
	}

}
