package net.tardis.mod.misc;

import net.minecraft.util.math.BlockPos;

public class WaypointWrapper {

    public SpaceTimeCoord coord;
    public BlockPos bankPos;
    public int index;

    public WaypointWrapper(BlockPos bank, SpaceTimeCoord coord, int index) {
        this.coord = coord;
        this.bankPos = bank;
        this.index = index;
    }

}
