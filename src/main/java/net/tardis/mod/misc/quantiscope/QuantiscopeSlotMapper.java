package net.tardis.mod.misc.quantiscope;

import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.inventory.container.Slot;
import net.tardis.mod.containers.QuantiscopeContainer;
import net.tardis.mod.tileentities.machines.QuantiscopeTile;

public abstract class QuantiscopeSlotMapper {

    public abstract void addSlots(QuantiscopeContainer container, QuantiscopeTile tile);

    public void addPlayerSlots(QuantiscopeContainer container, PlayerInventory inv){
        //Add Main inv
        for(int row = 0; row < 3; ++row) {
            for(int slot = 0; slot < 9; ++slot) {
                container.addSlot(new Slot(inv, slot + row * 9 + 9, 8 + slot * 18, 81 + row * 18 + 3));
            }
        }

        //Add hot bar
        for(int i1 = 0; i1 < 9; ++i1) {
            container.addSlot(new Slot(inv, i1, 8 + i1 * 18, 139 + 3));
        }
    }

}
