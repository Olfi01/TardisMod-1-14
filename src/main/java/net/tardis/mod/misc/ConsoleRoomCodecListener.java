package net.tardis.mod.misc;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.logging.log4j.Logger;

import com.google.gson.JsonElement;
import com.mojang.serialization.Codec;
import com.mojang.serialization.JsonOps;

import net.minecraft.util.ResourceLocation;
import net.tardis.mod.ars.ConsoleRoom;

public class ConsoleRoomCodecListener extends CodecJsonDataListener<ConsoleRoom> {

	public ConsoleRoomCodecListener(String folderName, Codec<ConsoleRoom> codec, Logger logger) {
		super(folderName, codec, logger);
	}

	@Override
	public Map<ResourceLocation, ConsoleRoom> mapValues(Map<ResourceLocation, JsonElement> inputs) {
		Map<ResourceLocation, ConsoleRoom> map = ConsoleRoom.registerCoreConsoleRooms();
		Map<ResourceLocation, ConsoleRoom> parsedInData = new HashMap<>();
		for (Entry<ResourceLocation, JsonElement> entry : inputs.entrySet()){
			ResourceLocation key = entry.getKey();
			JsonElement element = entry.getValue();
			// if we fail to parse json, log an error and continue
			// if we succeeded, add the resulting ConsoleRoom to the map
			this.codec.decode(JsonOps.INSTANCE, element)
				.get()
				.ifLeft(result -> {result.getFirst().setRegistryName(key); parsedInData.put(key, result.getFirst()); this.logger.info("Added Datapack entry: {}", key.toString());})
				.ifRight(partial -> this.logger.error("Failed to parse data json for {} due to: {}", key.toString(), partial.message()));
		}
		
		map.putAll(parsedInData);
		return map;
	}
	
	@Override
	public void setData(Map<ResourceLocation, ConsoleRoom> input) {
		for (Entry<ResourceLocation, ConsoleRoom> entry : input.entrySet()) {
			entry.getValue().setRegistryName(entry.getKey());
		}
		super.setData(input);
	}

}
