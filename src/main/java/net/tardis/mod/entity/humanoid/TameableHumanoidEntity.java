package net.tardis.mod.entity.humanoid;

import java.util.Optional;
import java.util.UUID;

import net.minecraft.entity.CreatureEntity;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.network.datasync.DataParameter;
import net.minecraft.network.datasync.DataSerializers;
import net.minecraft.network.datasync.EntityDataManager;
import net.minecraft.util.ActionResultType;
import net.minecraft.util.Hand;
import net.minecraft.util.ResourceLocation;
import net.minecraft.world.World;
import net.minecraft.world.server.ServerWorld;
import net.tardis.mod.entity.IPlayerTameable;
import net.tardis.mod.misc.ObjectWrapper;
import net.tardis.mod.missions.misc.Dialog;
/** Template for tameable humanoid entity. 
 * <br> Has a generic Tamed data parameter which can hold substates of:
 * <p> -Tamed
 * <br> -Sleeping*/
public class TameableHumanoidEntity extends AbstractHumanoidEntity implements IPlayerTameable{

	public static final DataParameter<Optional<UUID>> OWNER = EntityDataManager.createKey(TameableHumanoidEntity.class, DataSerializers.OPTIONAL_UNIQUE_ID);
	public static final DataParameter<Boolean> SITTING = EntityDataManager.createKey(TameableHumanoidEntity.class, DataSerializers.BOOLEAN);
	protected PlayerEntity ownerPlayer;
	
	public TameableHumanoidEntity(EntityType<? extends CreatureEntity> type, World worldIn) {
		super(type, worldIn);

	}
	
	@Override
	protected void registerData() {
		super.registerData();
		this.getDataManager().register(OWNER, Optional.empty());
		this.getDataManager().register(SITTING, false);
	}

	@Override
	public void readAdditional(CompoundNBT compound) {
		super.readAdditional(compound);
		if (compound.contains("owner_id"))
		    this.dataManager.set(OWNER, Optional.of(compound.getUniqueId("owner_id")));
		setSitting(compound.getBoolean("is_sitting"));
	}

	@Override
	public void writeAdditional(CompoundNBT compound) {
		super.writeAdditional(compound);
		this.getDataManager().get(OWNER).ifPresent(owner -> {
			compound.putUniqueId("owner_id", owner);
		});
		compound.putBoolean("is_sitting", this.isSitting());
	}

	@Override
	public Dialog getCurrentDialog(PlayerEntity player) {
		return null;
	}

	@Override
	public boolean isTamed() {
		ObjectWrapper<Boolean> tamed = new ObjectWrapper<>(false);
		this.dataManager.get(OWNER).ifPresent(owner -> {
			tamed.setValue(true);
		});
		return tamed.getValue();
	}

	@Override
	public UUID getOwner() {
		return this.dataManager.get(OWNER).orElse(null);
	}

	@Override
	public void setOwner(UUID owner) {
		this.dataManager.set(OWNER, Optional.of(owner));
	}
	
	public PlayerEntity getOwnerEntity() {

		UUID ownerId = this.getOwner();

		if(ownerId == null)
			return null;
		
		if(this.ownerPlayer != null && this.ownerPlayer.isAlive())
			return ownerPlayer;

		return this.ownerPlayer = world.getPlayerByUuid(ownerId);
	}

	@Override
	public ResourceLocation getSkin() {
		return new ResourceLocation("textures/entity/steve.png");
	}

	@Override
	public boolean isSitting() {
		return this.dataManager.get(SITTING);
	}

	@Override
	public void setSitting(boolean sit) {
		this.dataManager.set(SITTING, sit);
	}

}
