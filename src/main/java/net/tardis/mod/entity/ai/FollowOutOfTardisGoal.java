package net.tardis.mod.entity.ai;


import java.util.Optional;

import com.google.common.collect.Lists;

import net.minecraft.entity.CreatureEntity;
import net.minecraft.entity.Entity;
import net.minecraft.util.concurrent.TickDelayedTask;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.tardis.mod.entity.DoorEntity;
import net.tardis.mod.enums.EnumDoorState;

public class FollowOutOfTardisGoal extends FollowIntoTardisGoal{

	public FollowOutOfTardisGoal(CreatureEntity entity, double speed) {
		super(entity, speed);
	}

	@Override
	public void tick() {
		
		if(this.entity.getNavigator().noPath()) {
			entity.getNavigator().tryMoveToXYZ(this.target.getX() + 0.5, this.target.getY() + 1, this.target.getZ() + 0.5, 1.2);
		}
		
		if(this.entity.getPosition().withinDistance(this.target, 2)) {
			
			AxisAlignedBB box = new AxisAlignedBB(this.target).grow(3D);
			Optional<Entity> optEntity = this.entity.world.getLoadedEntitiesWithinAABB(Entity.class, box).stream().filter(entity -> entity instanceof DoorEntity && this.entity.getPosition().withinDistance(entity.getPosition(), 16)).findFirst();
			entity.world.getServer().enqueue(new TickDelayedTask(0, () -> optEntity.ifPresent(
					door -> {
						DoorEntity doorEnt = (DoorEntity)door;
						if (doorEnt.getOpenState() != EnumDoorState.CLOSED && !doorEnt.isLocked() && !doorEnt.isDeadLocked()) {
							doorEnt.teleportEntities(Lists.newArrayList(entity));
						}
			        })
				)
			);
			this.target = BlockPos.ZERO;
		}
		
		if(this.timeout > 0)
			--this.timeout;
	}

	
}
