package net.tardis.mod.entity.misc.outfits;

import net.minecraft.inventory.EquipmentSlotType;
import net.tardis.mod.cap.Capabilities;
import net.tardis.mod.cap.ISpaceDimProperties;
import net.tardis.mod.items.TItems;

public class SpaceSuitOutfit extends Outfit{

    public SpaceSuitOutfit() {
        super(ent -> {
            ISpaceDimProperties prop = ent.getEntityWorld().getCapability(Capabilities.SPACE_DIM_PROPERTIES).orElse(null);
            return prop != null && !prop.hasAir();
        });
    }

    @Override
    public void registerItems() {
        this.addItem(EquipmentSlotType.HEAD, TItems.SPACE_HELM.get());
        this.addItem(EquipmentSlotType.CHEST, TItems.SPACE_CHEST.get());
        this.addItem(EquipmentSlotType.LEGS, TItems.SPACE_LEGS.get());
        this.addItem(EquipmentSlotType.FEET, TItems.SPACE_BOOTS.get());
    }
}
