package net.tardis.mod.client.models.exteriors;
// Made with Blockbench 3.7.4
// Exported for Minecraft version 1.15
// Paste this class into your mod and generate all required imports

import com.mojang.blaze3d.matrix.MatrixStack;
import com.mojang.blaze3d.vertex.IVertexBuilder;

import net.minecraft.client.renderer.model.ModelRenderer;
import net.minecraft.client.world.ClientWorld;
import net.minecraft.entity.Entity;
import net.minecraft.util.math.vector.Vector3f;
import net.tardis.mod.client.ClientHelper;
import net.tardis.mod.client.TRenderTypes;
import net.tardis.mod.client.models.LightModelRenderer;
import net.tardis.mod.client.renderers.boti.BOTIRenderer;
import net.tardis.mod.client.renderers.boti.PortalInfo;
import net.tardis.mod.client.renderers.exteriors.BrokenExteriorRenderer.IBrokenExteriorModel;
import net.tardis.mod.client.renderers.exteriors.ExteriorRenderer;
import net.tardis.mod.client.renderers.exteriors.SafeExteriorRenderer;
import net.tardis.mod.enums.EnumDoorState;
import net.tardis.mod.enums.EnumMatterState;
import net.tardis.mod.helper.Helper;
import net.tardis.mod.helper.WorldHelper;
import net.tardis.mod.misc.IDoorType.EnumDoorType;
import net.tardis.mod.tileentities.exteriors.ExteriorTile;

public class JapanExteriorModel extends ExteriorModel implements IBrokenExteriorModel{
    private final ModelRenderer Roof;
    private final ModelRenderer Main;
    private final ModelRenderer cube_r1;
    private final ModelRenderer cube_r2;
    private final ModelRenderer cube_r3;
    private final ModelRenderer cube_r4;
    private final ModelRenderer Connectors;
    private final ModelRenderer cube_r5;
    private final ModelRenderer cube_r6;
    private final ModelRenderer cube_r7;
    private final ModelRenderer cube_r8;
    private final ModelRenderer Corners;
    private final ModelRenderer cube_r9;
    private final ModelRenderer cube_r10;
    private final ModelRenderer cube_r11;
    private final ModelRenderer cube_r12;
    private final ModelRenderer cube_r13;
    private final ModelRenderer cube_r14;
    private final ModelRenderer cube_r15;
    private final ModelRenderer cube_r16;
    private final ModelRenderer Posts;
    private final ModelRenderer cube_r17;
    private final ModelRenderer cube_r18;
    private final ModelRenderer cube_r19;
    private final ModelRenderer Signage;
    private final ModelRenderer panels;
    private final ModelRenderer lamp;
    private final ModelRenderer boti;
    private final ModelRenderer door;
    private final LightModelRenderer glow_lamp;
    private final ModelRenderer under_roof;

    public JapanExteriorModel() {
        textureWidth = 256;
        textureHeight = 256;

        Roof = new ModelRenderer(this);
        Roof.setRotationPoint(14.0F, -18.0F, -14.0F);
        

        Main = new ModelRenderer(this);
        Main.setRotationPoint(-14.0F, -9.0F, 14.0F);
        Roof.addChild(Main);
        

        cube_r1 = new ModelRenderer(this);
        cube_r1.setRotationPoint(15.0F, 7.0F, -3.0F);
        Main.addChild(cube_r1);
        setRotationAngle(cube_r1, -0.3491F, 1.5708F, 0.0F);
        cube_r1.setTextureOffset(0, 122).addBox(-17.0F, -1.0F, -16.0F, 28.0F, 1.0F, 16.0F, 0.0F, false);

        cube_r2 = new ModelRenderer(this);
        cube_r2.setRotationPoint(0.0F, 7.0F, 15.0F);
        Main.addChild(cube_r2);
        setRotationAngle(cube_r2, -0.3491F, 0.0F, 0.0F);
        cube_r2.setTextureOffset(0, 122).addBox(-14.0F, -1.0F, -16.0F, 28.0F, 1.0F, 16.0F, 0.0F, false);

        cube_r3 = new ModelRenderer(this);
        cube_r3.setRotationPoint(-15.0F, 7.0F, -15.0F);
        Main.addChild(cube_r3);
        setRotationAngle(cube_r3, -0.3491F, -1.5708F, 0.0F);
        cube_r3.setTextureOffset(0, 122).addBox(1.0F, -1.0F, -16.0F, 28.0F, 1.0F, 16.0F, 0.0F, false);

        cube_r4 = new ModelRenderer(this);
        cube_r4.setRotationPoint(0.0F, 7.0F, -15.0F);
        Main.addChild(cube_r4);
        setRotationAngle(cube_r4, -0.3491F, 3.1416F, 0.0F);
        cube_r4.setTextureOffset(0, 122).addBox(-14.0F, -1.0F, -16.0F, 28.0F, 1.0F, 16.0F, 0.0F, false);

        Connectors = new ModelRenderer(this);
        Connectors.setRotationPoint(-14.0F, -9.0F, 14.0F);
        Roof.addChild(Connectors);
        setRotationAngle(Connectors, 0.0F, 0.7854F, 0.0F);
        

        cube_r5 = new ModelRenderer(this);
        cube_r5.setRotationPoint(0.0F, 1.0F, 0.0F);
        Connectors.addChild(cube_r5);
        setRotationAngle(cube_r5, 0.2618F, -1.5708F, 0.0F);
        cube_r5.setTextureOffset(38, 146).addBox(-1.0F, -1.0F, -22.0F, 2.0F, 1.0F, 19.0F, 0.0F, false);

        cube_r6 = new ModelRenderer(this);
        cube_r6.setRotationPoint(0.0F, 1.0F, 0.0F);
        Connectors.addChild(cube_r6);
        setRotationAngle(cube_r6, 0.2618F, 1.5708F, 0.0F);
        cube_r6.setTextureOffset(38, 146).addBox(-1.0F, -1.0F, -22.0F, 2.0F, 1.0F, 19.0F, 0.0F, false);

        cube_r7 = new ModelRenderer(this);
        cube_r7.setRotationPoint(0.0F, 1.0F, 0.0F);
        Connectors.addChild(cube_r7);
        setRotationAngle(cube_r7, 0.2618F, 3.1416F, 0.0F);
        cube_r7.setTextureOffset(38, 146).addBox(-1.0F, -1.0F, -22.0F, 2.0F, 1.0F, 19.0F, 0.0F, false);

        cube_r8 = new ModelRenderer(this);
        cube_r8.setRotationPoint(0.0F, 1.0F, 0.0F);
        Connectors.addChild(cube_r8);
        setRotationAngle(cube_r8, 0.2618F, 0.0F, 0.0F);
        cube_r8.setTextureOffset(38, 146).addBox(-1.0F, -1.0F, -22.0F, 2.0F, 1.0F, 19.0F, 0.0F, false);

        Corners = new ModelRenderer(this);
        Corners.setRotationPoint(-14.0F, 42.0F, 14.0F);
        Roof.addChild(Corners);
        setRotationAngle(Corners, 0.0F, 0.7854F, 0.0F);
        

        cube_r9 = new ModelRenderer(this);
        cube_r9.setRotationPoint(0.0F, -48.1079F, 0.0F);
        Corners.addChild(cube_r9);
        setRotationAngle(cube_r9, 0.0F, 1.5708F, 0.0F);
        cube_r9.setTextureOffset(127, 35).addBox(-0.5F, 3.25F, -21.7372F, 1.0F, 3.0F, 1.0F, 0.0F, false);

        cube_r10 = new ModelRenderer(this);
        cube_r10.setRotationPoint(0.0F, -48.1079F, 0.0F);
        Corners.addChild(cube_r10);
        setRotationAngle(cube_r10, 0.0F, -1.5708F, 0.0F);
        cube_r10.setTextureOffset(127, 35).addBox(-0.5F, 3.25F, -21.7372F, 1.0F, 3.0F, 1.0F, 0.0F, false);

        cube_r11 = new ModelRenderer(this);
        cube_r11.setRotationPoint(0.0F, -48.1079F, 0.0F);
        Corners.addChild(cube_r11);
        setRotationAngle(cube_r11, 0.0F, 0.0F, 0.0F);
        cube_r11.setTextureOffset(127, 35).addBox(-0.5F, 3.25F, -21.7372F, 1.0F, 3.0F, 1.0F, 0.0F, false);

        cube_r12 = new ModelRenderer(this);
        cube_r12.setRotationPoint(0.0F, -48.1079F, 0.0F);
        Corners.addChild(cube_r12);
        setRotationAngle(cube_r12, 0.0F, 3.1416F, 0.0F);
        cube_r12.setTextureOffset(127, 35).addBox(-0.5F, 3.25F, -21.7372F, 1.0F, 3.0F, 1.0F, 0.0F, false);

        cube_r13 = new ModelRenderer(this);
        cube_r13.setRotationPoint(0.0F, -48.1442F, -0.366F);
        Corners.addChild(cube_r13);
        setRotationAngle(cube_r13, 0.0F, 1.5708F, 0.3491F);
        cube_r13.setTextureOffset(39, 157).addBox(-1.0F, 10.0F, -21.0F, 2.0F, 1.0F, 7.0F, 0.0F, false);

        cube_r14 = new ModelRenderer(this);
        cube_r14.setRotationPoint(0.0F, -48.1442F, -0.366F);
        Corners.addChild(cube_r14);
        setRotationAngle(cube_r14, 0.0F, -1.5708F, -0.3491F);
        cube_r14.setTextureOffset(39, 157).addBox(-1.0F, 10.0F, -21.0F, 2.0F, 1.0F, 7.0F, 0.0F, false);

        cube_r15 = new ModelRenderer(this);
        cube_r15.setRotationPoint(0.0F, -48.1442F, -0.366F);
        Corners.addChild(cube_r15);
        setRotationAngle(cube_r15, -0.3491F, 3.1416F, 0.0F);
        cube_r15.setTextureOffset(39, 157).addBox(-1.0F, 10.0F, -21.0F, 2.0F, 1.0F, 7.0F, 0.0F, false);
        cube_r15.setTextureOffset(39, 157).addBox(-1.0F, 10.0F, -21.0F, 2.0F, 1.0F, 7.0F, 0.0F, false);

        cube_r16 = new ModelRenderer(this);
        cube_r16.setRotationPoint(0.0F, -48.1442F, -0.366F);
        Corners.addChild(cube_r16);
        setRotationAngle(cube_r16, -0.3491F, 0.0F, 0.0F);
        cube_r16.setTextureOffset(39, 157).addBox(-1.0F, 10.0F, -21.0F, 2.0F, 1.0F, 7.0F, 0.0F, false);

        Posts = new ModelRenderer(this);
        Posts.setRotationPoint(-12.0F, 20.0F, 10.0F);
        Posts.setTextureOffset(131, 31).addBox(22.0F, -41.0F, -23.0F, 3.0F, 41.0F, 3.0F, 0.0F, false);
        Posts.setTextureOffset(0, 0).addBox(-2.0F, 0.0F, -24.0F, 28.0F, 4.0F, 28.0F, 0.0F, false);

        cube_r17 = new ModelRenderer(this);
        cube_r17.setRotationPoint(12.0F, 0.0F, -10.0F);
        Posts.addChild(cube_r17);
        setRotationAngle(cube_r17, 0.0F, 1.5708F, 0.0F);
        cube_r17.setTextureOffset(131, 31).addBox(10.0F, -41.0F, -13.0F, 3.0F, 41.0F, 3.0F, 0.0F, false);

        cube_r18 = new ModelRenderer(this);
        cube_r18.setRotationPoint(12.0F, 0.0F, -10.0F);
        Posts.addChild(cube_r18);
        setRotationAngle(cube_r18, 0.0F, -1.5708F, 0.0F);
        cube_r18.setTextureOffset(131, 31).addBox(10.0F, -41.0F, -13.0F, 3.0F, 41.0F, 3.0F, 0.0F, false);

        cube_r19 = new ModelRenderer(this);
        cube_r19.setRotationPoint(12.0F, 0.0F, -10.0F);
        Posts.addChild(cube_r19);
        setRotationAngle(cube_r19, -3.1416F, 0.0F, 3.1416F);
        cube_r19.setTextureOffset(131, 31).addBox(10.0F, -41.0F, -13.0F, 3.0F, 41.0F, 3.0F, 0.0F, false);

        Signage = new ModelRenderer(this);
        Signage.setRotationPoint(14.0F, -18.0F, -14.0F);
        Signage.setTextureOffset(32, 78).addBox(-26.0F, -1.0F, 0.0F, 24.0F, 4.0F, 3.0F, 0.0F, false);
        Signage.setTextureOffset(42, 87).addBox(-28.0F, -1.0F, 2.0F, 3.0F, 4.0F, 24.0F, 0.0F, false);
        Signage.setTextureOffset(32, 78).addBox(-26.0F, -1.0F, 25.0F, 24.0F, 4.0F, 3.0F, 0.0F, false);
        Signage.setTextureOffset(42, 87).addBox(-3.0F, -1.0F, 2.0F, 3.0F, 4.0F, 24.0F, 0.0F, false);

        panels = new ModelRenderer(this);
        panels.setRotationPoint(9.0F, 20.0F, -12.0F);
        panels.setTextureOffset(88, 32).addBox(2.0F, -35.0F, 2.0F, 1.0F, 35.0F, 20.0F, 0.0F, false);
        panels.setTextureOffset(88, 32).addBox(-21.0F, -35.0F, 2.0F, 1.0F, 35.0F, 20.0F, 0.0F, false);
        panels.setTextureOffset(0, 86).addBox(-19.0F, -35.0F, 23.0F, 20.0F, 35.0F, 1.0F, 0.0F, false);
        panels.setTextureOffset(118, 87).addBox(-19.0F, -35.0F, 0.0F, 10.0F, 35.0F, 1.0F, 0.0F, false);

        lamp = new ModelRenderer(this);
        lamp.setRotationPoint(14.0F, -26.2F, -14.0F);
        lamp.setTextureOffset(147, 33).addBox(-17.0F, -1.0F, 11.0F, 6.0F, 2.0F, 6.0F, 0.0F, false);
        lamp.setTextureOffset(147, 41).addBox(-17.0F, -6.0F, 11.0F, 6.0F, 1.0F, 6.0F, 0.0F, false);
        lamp.setTextureOffset(145, 70).addBox(-16.0F, -8.0F, 12.0F, 4.0F, 2.0F, 4.0F, 0.0F, false);
        lamp.setTextureOffset(153, 76).addBox(-15.0F, -10.0F, 13.0F, 2.0F, 2.0F, 2.0F, 0.0F, false);
        lamp.setTextureOffset(161, 76).addBox(-12.5F, -5.0F, 11.5F, 1.0F, 4.0F, 1.0F, 0.0F, false);
        lamp.setTextureOffset(161, 76).addBox(-16.5F, -5.0F, 11.5F, 1.0F, 4.0F, 1.0F, 0.0F, false);
        lamp.setTextureOffset(161, 76).addBox(-16.5F, -5.0F, 15.5F, 1.0F, 4.0F, 1.0F, 0.0F, false);
        lamp.setTextureOffset(161, 76).addBox(-12.5F, -5.0F, 15.5F, 1.0F, 4.0F, 1.0F, 0.0F, false);

        boti = new ModelRenderer(this);
        boti.setRotationPoint(0.0F, 24.0F, 0.0F);
        boti.setTextureOffset(211, 38).addBox(-10.0F, -41.0F, -9.5F, 20.0F, 37.0F, 1.0F, 0.0F, false);

        door = new ModelRenderer(this);
        door.setRotationPoint(0.0F, 20.0F, 10.0F);
        door.setTextureOffset(96, 87).addBox(0.0F, -35.0F, -21.0F, 10.0F, 35.0F, 1.0F, 0.0F, false);

        glow_lamp = new LightModelRenderer(this);
        glow_lamp.setRotationPoint(1.0F, -25.0F, 0.0F);
        glow_lamp.setTextureOffset(147, 48).addBox(-3.0F, -6.2F, -2.0F, 4.0F, 4.0F, 4.0F, 0.0F, false);

        under_roof = new ModelRenderer(this);
        under_roof.setRotationPoint(0.0F, 24.0F, 0.0F);
        under_roof.setTextureOffset(117, 0).addBox(-12.0F, -45.0F, -12.0F, 24.0F, 2.0F, 24.0F, 0.0F, false);
    }

    @Override
    public void setRotationAngles(Entity entity, float limbSwing, float limbSwingAmount, float ageInTicks, float netHeadYaw, float headPitch){
        //previously the render function, render code was moved to a method below
    }

    @Override
    public void render(MatrixStack matrixStack, IVertexBuilder buffer, int packedLight, int packedOverlay, float red, float green, float blue, float alpha){
        
    }

    public void setRotationAngle(ModelRenderer modelRenderer, float x, float y, float z) {
        modelRenderer.rotateAngleX = x;
        modelRenderer.rotateAngleY = y;
        modelRenderer.rotateAngleZ = z;
    }

    @Override
    public void renderBones(ExteriorTile exterior, float scale, MatrixStack matrixStack, IVertexBuilder buffer, int packedLight, int packedOverlay, float alpha) {
        matrixStack.push();
        matrixStack.translate(EnumDoorType.JAPAN.getRotationForState(exterior.getOpen()), 0, 0);
        door.render(matrixStack, buffer, packedLight, packedOverlay,1,1,1, alpha);
        matrixStack.pop();
        
        Roof.render(matrixStack, buffer, packedLight, packedOverlay,1,1,1, alpha);
        Posts.render(matrixStack, buffer, packedLight, packedOverlay,1,1,1, alpha);
        Signage.render(matrixStack, buffer, packedLight, packedOverlay,1,1,1, alpha);
        panels.render(matrixStack, buffer, packedLight, packedOverlay,1,1,1, alpha);
        lamp.render(matrixStack, buffer, packedLight, packedOverlay,1,1,1, alpha);
        glow_lamp.setBright(exterior.getLightLevel() >= 1 ? 1F : exterior.getLightLevel() * 10F);
        glow_lamp.render(matrixStack, buffer, packedLight, packedOverlay,1,1,1, alpha);
        
        under_roof.render(matrixStack, buffer, packedLight, packedOverlay,1,1,1, alpha);
    }

    @Override
    public void renderBoti(ExteriorTile exterior, float scale, MatrixStack matrixStack, IVertexBuilder buffer,
            int packedLight, int packedOverlay, float alpha) {
        if(exterior.getBotiWorld() != null && exterior.getMatterState() == EnumMatterState.SOLID && exterior.getOpen() != EnumDoorState.CLOSED) {

            PortalInfo info = new PortalInfo();

            info.setPosition(exterior.getPos());
            info.setWorldShell(exterior.getBotiWorld());

            info.setTranslate(matrix -> {
                matrix.translate(-0.5, 0, -0.5);
                ExteriorRenderer.applyTransforms(matrix, exterior);
                matrix.translate(0.4, -0.85, 0.05);
            });

            info.setTranslatePortal(matrix -> {
                matrix.rotate(Vector3f.ZN.rotationDegrees(180));
                matrix.rotate(Vector3f.YP.rotationDegrees(WorldHelper.getAngleFromFacing(exterior.getBotiWorld().getPortalDirection())));
                matrix.translate(-0.5, -1.5, -0.5);
            });

            info.setRenderPortal((matrix, buf) -> {
                matrix.push();
                matrix.scale(1.125F, 1.125F, 1.125F);
                this.boti.render(matrix, buf.getBuffer(TRenderTypes.getTardis(Helper.getVariantTextureOr(exterior.getVariant(), SafeExteriorRenderer.TEXTURE))), packedLight, packedOverlay);
                matrix.pop();
            });

            BOTIRenderer.addPortal(info);
        }
    }

    @Override
    public void renderBrokenExterior(MatrixStack matrix, IVertexBuilder buffer, int combinedLight,
            int combinedOverlay) {
        matrix.push();
        matrix.translate(0, -1, 0);
        door.render(matrix, buffer, combinedLight, combinedOverlay);
        Roof.render(matrix, buffer, combinedLight, combinedOverlay);
        Posts.render(matrix, buffer, combinedLight, combinedOverlay);
        Signage.render(matrix, buffer, combinedLight, combinedOverlay);
        panels.render(matrix, buffer, combinedLight, combinedOverlay);
        lamp.render(matrix, buffer, combinedLight, combinedOverlay);
        ClientWorld world = ClientHelper.getClientWorldCasted();
        float flickeringLight = 0;
        if(world.rand.nextBoolean()){
            flickeringLight = world.rand.nextFloat();
        }
        glow_lamp.setBright(flickeringLight);
        glow_lamp.render(matrix, buffer, combinedLight, combinedOverlay);
        under_roof.render(matrix, buffer, combinedLight, combinedOverlay);
        matrix.pop();
    }
}