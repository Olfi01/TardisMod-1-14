package net.tardis.mod.client.guis.minigame.wires;

import java.awt.Color;

import net.minecraft.client.renderer.BufferBuilder;
import net.minecraft.util.math.vector.Vector3i;
import net.tardis.mod.helper.Helper;

public class Wire {

	public static final int SIZE_X = 16;
	public static final int SIZE_Y = 14;
	private Type type;
	public Vector3i startPos;
	public Vector3i endPos;
	private boolean selected = false;
	private boolean complete = false;
	
	public Wire(Type type, int x, int y) {
		this.type = type;
		this.startPos = this.endPos = new Vector3i(x, y, 0);
	}
	
	public void update(int mouseX, int mouseY) {
		if(this.isSelected())
			this.endPos = new Vector3i(mouseX, mouseY, 0);
	}
	
	public void render(BufferBuilder bb) {
		
		int size = 5;
		
		int x = this.startPos.getX() + SIZE_X - 1, endX = this.endPos.getX();
		int y = this.startPos.getY() + SIZE_Y / 2 - size / 2, endY = this.endPos.getY();
		
		float red = this.type.color.getRed() / 255.0F;
		float green = this.type.color.getGreen() / 255.0F;
		float blue = this.type.color.getBlue() / 255.0F;
		
		bb.pos(x, y, 0).color(red, green, blue, 1F).endVertex();
		bb.pos(x, y + size, 0).color(red, green, blue, 1F).endVertex();
		bb.pos(endX + size, endY + size, 0).color(red, green, blue, 1F).endVertex();
		bb.pos(endX + size, endY, 0).color(red, green, blue, 1F).endVertex();

	}
	
	public void renderPost(BufferBuilder bb) {
		int x = this.startPos.getX(), y = this.startPos.getY();
		
		float minU = (float) type.getMinU();
		float maxU = (float) type.getMaxU();
		float minV = (float) type.getMinV();
		float maxV = (float) type.getMaxV();
		
		bb.pos(x, y, 0).tex(minU, minV).endVertex();
		bb.pos(x, y + SIZE_Y, 0).tex(minU, maxV).endVertex();
		bb.pos(x + SIZE_X, y + SIZE_Y, 0).tex(maxU, maxV).endVertex();
		bb.pos(x + SIZE_X, y, 0).tex(maxU, minV).endVertex();
	}
	
	public void unselect() {
		this.endPos = this.startPos;
		this.selected = false;
	}
	
	public void onClick(int mouseX, int mouseY) {
		if(!this.complete) {
			if(Helper.isInBounds(mouseX, mouseY, this.startPos.getX(), this.startPos.getY(), this.startPos.getX() + SIZE_X, this.startPos.getY() + SIZE_Y)) {
				this.selected = true;
			}
			else this.unselect();
		}
	}
	
	public boolean isSelected() {
		return this.selected;
	}
	
	public boolean isComplete() {
		return this.complete;
	}
	
	public Type getType() {
		return this.type;
	}
	
	public void setComplete(boolean complete) {
		this.complete = complete;
		this.selected = false;
	}
	
	public void setEndPos(Vector3i end) {
		this.endPos = end;
	}

	
	
	public static enum Type{
		RED(0x9d0b26, 2, 240),
		GREEN(0x86dd85, 20, 240),
		BLACK(0x000000, 38, 240),
		BLUE(0x397dac, 56, 240);
		
		Color color;
		private double u, v;
		
		Type(int hex, int u, int v){
			this(new Color(hex), u, v);
		}
		
		Type(Color color, int u, int v){
			this.color = color;
			this.u = u;
			this.v = v;
		}
		
		public double getMinU() {
			return u / 256.0;
		}
		
		public double getMinV() {
			return v / 256.0;
		}
		
		public double getMaxU() {
			return (u + Wire.SIZE_X) / 256.0;
		}
		
		public double getMaxV() {
			return (v + Wire.SIZE_Y) / 256.0;
		}
	}
}
