package net.tardis.mod.client.guis.containers;

import com.mojang.blaze3d.matrix.MatrixStack;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.screen.inventory.ContainerScreen;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.inventory.container.Container;
import net.minecraft.util.text.TranslationTextComponent;
import net.tardis.mod.Tardis;
import net.tardis.mod.client.TClientRegistry;
import net.tardis.mod.client.guis.misc.QuantiscopePage;
import net.tardis.mod.misc.quantiscope.QuantiscopeScreenType;

public abstract class QuantiscopeScreen<T extends Container> extends ContainerScreen<T> {

    private QuantiscopePage page;
    private QuantiscopeScreenType type;

    public QuantiscopeScreen(T screenContainer, PlayerInventory inv, String nameSuffix) {
        super(screenContainer, inv, new TranslationTextComponent("container." + Tardis.MODID + ".quantiscope." + nameSuffix));
    }

    public void setQuantiscopeScreenType(QuantiscopeScreenType type){
        this.type = type;
        this.page = TClientRegistry.QUANTISCOPE_SCREENS.get(type);
    }

    @Override
    protected void drawGuiContainerBackgroundLayer(MatrixStack matrixStack, float partialTicks, int x, int y) {

       if(this.page != null){
           Minecraft.getInstance().textureManager.bindTexture(this.page.getTexture());
           matrixStack.push();
           this.blit(matrixStack, this.width / 2 - page.getWidth() / 2, this.height / 2 - page.getHeight() / 2, 0, 0, page.getWidth(), page.getHeight());
           matrixStack.pop();
       }

    }
}
