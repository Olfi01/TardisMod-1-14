package net.tardis.mod.client.guis.widgets;

import com.mojang.blaze3d.matrix.MatrixStack;

import net.minecraft.client.gui.widget.Widget;
import net.minecraft.util.text.ITextComponent;

public class RotatingKnob extends Widget {

    private float rotation;

    public RotatingKnob(int x, int y, ITextComponent title) {
        super(x, y, 16, 16, title);
    }
    //TODO: Port to 1.16 rendering
    @Override
    public void renderWidget(MatrixStack matrixStack, int x, int y, float partialTicks) {
        //BufferBuilder bb = Tessellator.getInstance().getBuffer();
        //bb.begin(GL11.GL_QUADS, DefaultVertexFormats.POSITION_TEX);
    }

    @Override
    public boolean mouseDragged(double p_mouseDragged_1_, double p_mouseDragged_3_, int p_mouseDragged_5_, double p_mouseDragged_6_, double p_mouseDragged_8_) {
        // TODO Auto-generated method stub
        return super.mouseDragged(p_mouseDragged_1_, p_mouseDragged_3_, p_mouseDragged_5_, p_mouseDragged_6_, p_mouseDragged_8_);
    }

    public float getRotation() {
        return this.rotation;
    }

    public void setRotation(float rot) {
        this.rotation = rot;
    }

}
