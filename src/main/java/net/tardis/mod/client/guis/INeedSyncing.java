package net.tardis.mod.client.guis;

import java.util.Map;

public interface INeedSyncing {
	void setStatesFromServer(Map<Integer, Boolean> upgradeStates);
}
