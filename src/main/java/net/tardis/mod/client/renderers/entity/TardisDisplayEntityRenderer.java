package net.tardis.mod.client.renderers.entity;

import com.mojang.blaze3d.matrix.MatrixStack;

import net.minecraft.client.renderer.IRenderTypeBuffer;
import net.minecraft.client.renderer.entity.EntityRenderer;
import net.minecraft.client.renderer.entity.EntityRendererManager;
import net.minecraft.client.renderer.texture.OverlayTexture;
import net.minecraft.client.renderer.tileentity.TileEntityRenderer;
import net.minecraft.client.renderer.tileentity.TileEntityRendererDispatcher;
import net.minecraft.util.ResourceLocation;
import net.tardis.mod.client.renderers.exteriors.ExteriorRenderer;
import net.tardis.mod.entity.TardisDisplayEntity;
import net.tardis.mod.tileentities.exteriors.ExteriorTile;

public class TardisDisplayEntityRenderer extends EntityRenderer<TardisDisplayEntity>{

    public TardisDisplayEntityRenderer(EntityRendererManager renderManager) {
        super(renderManager);
    }

    @Override
    public ResourceLocation getEntityTexture(TardisDisplayEntity entity) {
        return null;
    }

    @Override
    public void render(TardisDisplayEntity entity, float entityYaw, float partialTicks, MatrixStack matrixStackIn,
            IRenderTypeBuffer bufferIn, int packedLightIn) {
        matrixStackIn.push();
        matrixStackIn.translate(-0.5, 1.5, -0.5);
        if(entity.getTile() != null) {
        	TileEntityRendererDispatcher tileRendererDispatcher = TileEntityRendererDispatcher.instance;
        	TileEntityRenderer<ExteriorTile> tileRenderer = tileRendererDispatcher.getRenderer(entity.getTile());
            if (tileRenderer instanceof ExteriorRenderer) {
            	ExteriorRenderer<? extends ExteriorTile> extRenderer = (ExteriorRenderer<? extends ExteriorTile>)tileRenderer;
            	//Use a custom method to render the tile as an entity
            	//This is because the cachedBlockState of our tile entity will be null or Air if there's no block in the world for this tile, so the TileEntity renderer will not work
            	//Instead, we use a raw render method to achieve this.
            	extRenderer.renderExteriorAsEntity(entity.getTile(), partialTicks, matrixStackIn, bufferIn, packedLightIn, OverlayTexture.NO_OVERLAY);
            }
        }
        matrixStackIn.pop();
    }

}
