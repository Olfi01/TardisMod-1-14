package net.tardis.mod.items;

import java.util.List;

import net.minecraft.client.gui.screen.Screen;
import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.entity.Entity;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.item.ItemUseContext;
import net.minecraft.item.UseAction;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.util.ActionResult;
import net.minecraft.util.ActionResultType;
import net.minecraft.util.Hand;
import net.minecraft.util.NonNullList;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.SoundEvents;
import net.minecraft.util.math.RayTraceContext;
import net.minecraft.util.math.RayTraceContext.BlockMode;
import net.minecraft.util.math.RayTraceContext.FluidMode;
import net.minecraft.util.math.RayTraceResult;
import net.minecraft.util.math.vector.Vector3d;
import net.minecraft.util.text.IFormattableTextComponent;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.StringTextComponent;
import net.minecraft.util.text.TextFormatting;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraft.world.World;
import net.minecraftforge.common.ForgeMod;
import net.minecraftforge.items.ItemStackHandler;
import net.tardis.mod.artron.IArtronItemStackBattery;
import net.tardis.mod.client.TClientRegistry;
import net.tardis.mod.constants.TardisConstants;
import net.tardis.mod.helper.TInventoryHelper;
import net.tardis.mod.itemgroups.TItemGroups;
import net.tardis.mod.items.misc.LaserPowerTier;
import net.tardis.mod.items.misc.LaserWeapon;

public class LaserGunItem extends BaseItem {
    private ItemStackHandler handler;
    private final TranslationTextComponent NONE = new TranslationTextComponent("tooltip.laser_gun.charge.none");

    public LaserGunItem(int numSlots) {
        super(new Properties().maxStackSize(1).group(TItemGroups.MAIN));
        this.handler = new ItemStackHandler(numSlots);
    }

    public void addBattery(Entity gunHolder, ItemStack stack, ItemStack testStack) {
        CompoundNBT root = stack.getOrCreateTag();
        float chargeTotal = this.getTotalCharge(stack);
        int numBatteries = this.getNumBatteries(stack);
        if (!testStack.isEmpty()) {
            if (testStack.getItem() instanceof IArtronItemStackBattery) {
                IArtronItemStackBattery battery = (IArtronItemStackBattery)testStack.getItem();
                for (int index = 0 ; index < handler.getSlots(); index++) {
                    ItemStack slotStack = handler.getStackInSlot(index);
                    if (!testStack.isEmpty()) {
                        //Replace current battery item
                        if (slotStack.getItem() instanceof IArtronItemStackBattery) {
                            IArtronItemStackBattery slotBattery = (IArtronItemStackBattery)slotStack.getItem();
                            if (slotBattery.getCharge(slotStack) < battery.getCharge(testStack)) {
                                //Remove slot battery's charge from total
                                chargeTotal -= slotBattery.getCharge(slotStack);
                                TInventoryHelper.giveStackTo(gunHolder, slotStack.copy());
                                this.handler.setStackInSlot(index, ItemStack.EMPTY);
                                
                                //Add the new battery's value
                                chargeTotal += battery.getCharge(testStack);
                                this.handler.setStackInSlot(index, testStack.copy());
                                testStack.shrink(1);
                                gunHolder.world.playSound(null, gunHolder.getPosition(), SoundEvents.ITEM_ARMOR_EQUIP_GENERIC, SoundCategory.PLAYERS, 1F, 1F);
                            }
                        }
                        else {
                            //Add new battery
                            if (slotStack.isEmpty()) {
                                chargeTotal += battery.getCharge(testStack);
                                numBatteries ++;
                                this.handler.setStackInSlot(index, testStack.copy());
                                testStack.shrink(1);
                                gunHolder.world.playSound(null, gunHolder.getPosition(), SoundEvents.ITEM_ARMOR_EQUIP_GENERIC, SoundCategory.PLAYERS, 1F, 1F);
                            }
                        }                    
                    }
                }
            }
        }
        else {
            //Eject any empty batteries or batteries that don't have enough charge
            for (int index = 0 ; index < handler.getSlots(); index++) {
                ItemStack slotStack = handler.getStackInSlot(index);
                if (slotStack.getItem() instanceof IArtronItemStackBattery) {
                    IArtronItemStackBattery slotBattery = (IArtronItemStackBattery) slotStack.getItem();
                    float stackCharge = slotBattery.getCharge(slotStack);
                    float actualAmountToTake = this.getCurrentTier(stack).getChargeRequired();
                    boolean isCreativeBattery = false;
                    //If we have tardis mod artron batteries and their charge is below the amount it will be taking, eject them too
                    if (slotStack.getItem() instanceof ArtronItemStackBatteryItem) {
                        ArtronItemStackBatteryItem artronBattery = (ArtronItemStackBatteryItem)slotStack.getItem();
                        actualAmountToTake = artronBattery.getDischargeRate(this.getTierRequiredCharge(slotStack));
                        isCreativeBattery = artronBattery.isCreative();
                    }
                    if (!slotStack.isEmpty() && 
                            (stackCharge < actualAmountToTake || isCreativeBattery)) {
                        if (chargeTotal - actualAmountToTake >= 0) {
                            if (chargeTotal == Float.MAX_VALUE)
                                chargeTotal = 0;
                            else
                                chargeTotal -= actualAmountToTake;
                        }
                        else {
                            chargeTotal = 0;
                        }
                        
                        if (Float.toString(chargeTotal).equals("Infinity")) {
                            chargeTotal = 0;
                        }
                        
                        TInventoryHelper.giveStackTo(gunHolder, slotStack.copy());
                        this.handler.setStackInSlot(index, ItemStack.EMPTY);
                        if (numBatteries > 0)
                            numBatteries--;
                    }
                }
                
            }
        }
        root.put("inv", this.handler.serializeNBT());
        root.putFloat("charge", chargeTotal);
        root.putInt("num_batteries", numBatteries);
        this.calculateNumShots(stack);
    }
    
    public void removeCharge(Entity gunHolder, ItemStack stack, float amount) {
        CompoundNBT root = stack.getOrCreateTag();
        float chargeToDeplete = amount;
        float chargeTotal = this.getTotalCharge(stack);
        boolean shouldRemove = true;
        if (gunHolder instanceof PlayerEntity) {
            PlayerEntity player = (PlayerEntity)gunHolder;
            shouldRemove = !player.abilities.isCreativeMode;
        }
        if (shouldRemove) {
            for (int index = 0 ; index < handler.getSlots(); index++) {
                ItemStack slotStack = handler.getStackInSlot(index);
                if (slotStack.getItem() instanceof IArtronItemStackBattery) {
                	IArtronItemStackBattery battery = (IArtronItemStackBattery) slotStack.getItem();
                	float depleted = battery.discharge(slotStack, chargeToDeplete);
                	chargeToDeplete -= depleted;
                    if (chargeTotal - chargeToDeplete > 0)
                        chargeTotal -= chargeToDeplete;
                    else
                    	chargeTotal = 0;
                }
            }
        }
        root.put("inv", this.handler.serializeNBT());
        root.putFloat("charge", chargeTotal);
        this.calculateNumShots(stack);
    }

    public ItemStack getBatteryInInventory(NonNullList<ItemStack> stacks) {
        for (ItemStack stack : stacks) {
            if (stack.getItem() instanceof IArtronItemStackBattery)
                return stack;
        }
        return ItemStack.EMPTY;
    }
    
    public float getTotalCharge(ItemStack stack) {
        CompoundNBT root = stack.getOrCreateTag();
        if (root.contains("charge"))
            return root.getFloat("charge");
        return 0;
    }
    
    public int getNumBatteries(ItemStack stack) {
        CompoundNBT root = stack.getOrCreateTag();
        if (root.contains("num_batteries"))
            return root.getInt("num_batteries");
        return 0;
    }
    
    protected void calculateNumShots(ItemStack stack) {
         CompoundNBT root = stack.getOrCreateTag();
        int numShots = 0;
        for (int index = 0 ; index < handler.getSlots(); index++) {
            ItemStack slotStack = handler.getStackInSlot(index);
            if (slotStack.getItem() instanceof IArtronItemStackBattery) {
                IArtronItemStackBattery battery = (IArtronItemStackBattery)slotStack.getItem();
                float actualAmountToTake = this.getCurrentTier(stack).getChargeRequired();
                //Account for the discharge rate of Artron Battery item
                if (slotStack.getItem() instanceof ArtronItemStackBatteryItem) {
                    ArtronItemStackBatteryItem artronBattery = (ArtronItemStackBatteryItem)slotStack.getItem();
                    actualAmountToTake = artronBattery.getDischargeRate(this.getTierRequiredCharge(slotStack));
                }                
                numShots += Math.ceil((double)(battery.getCharge(slotStack) / actualAmountToTake));
            }
            
        }
        root.putInt("remaining_shots", numShots);
    }
    
    public int getNumShotsRemaining(ItemStack stack) {
        CompoundNBT root = stack.getOrCreateTag();
        if (root.contains("remaining_shots"))
            return root.getInt("remaining_shots");
        return 0;
    }
    
    public void setPowerTier(LaserPowerTier tier, ItemStack stack) {
        CompoundNBT root = stack.getOrCreateTag();
        root.putInt("power_tier", tier.ordinal());
        this.calculateNumShots(stack);
    }
    
    public LaserPowerTier getCurrentTier(ItemStack stack) {
        CompoundNBT root = stack.getOrCreateTag();
        if (root.contains("power_tier"))
           return LaserPowerTier.values()[(root.getInt("power_tier"))];
        return LaserPowerTier.LOW;
    }
    
    public float getTierRequiredCharge(ItemStack stack) {
        return this.getCurrentTier(stack).getChargeRequired();
    }

    @Override
    public UseAction getUseAction(ItemStack stack) {
        return UseAction.NONE;
    }

    @Override
    public ActionResultType onItemUse(ItemUseContext context) {
        if (!context.getWorld().isRemote()) {
            ItemStack gun = context.getItem();
            PlayerEntity player = context.getPlayer();
            if (player.isSneaking()) {
                int maxLength = LaserPowerTier.values().length;
                int nextIndex = getCurrentTier(gun).ordinal() + 1 < maxLength ? getCurrentTier(gun).ordinal() + 1 : 0;
                LaserPowerTier nextTier = LaserPowerTier.values()[nextIndex];
                this.setPowerTier(nextTier, gun);
                IFormattableTextComponent nextTierName = new TranslationTextComponent(nextTier.getTranslationKey()).mergeStyle(TextFormatting.LIGHT_PURPLE);
                player.sendStatusMessage(new TranslationTextComponent("message.laser_gun.set_power_tier").appendSibling(nextTierName), true);
                return ActionResultType.SUCCESS;
            }
        }
        return super.onItemUse(context);
    }

    @Override
    public ActionResult<ItemStack> onItemRightClick(World worldIn, PlayerEntity player, Hand handIn) {
        ItemStack gun = player.getHeldItem(handIn);
        if (!player.world.isRemote()) {
            if (player.isSneaking()) {
                Vector3d pos = player.getPositionVec().add(0, player.getEyeHeight(), 0);
                RayTraceResult result = worldIn.rayTraceBlocks(new RayTraceContext(pos, pos.add(player.getLookVec().scale((double)player.getAttributeValue(ForgeMod.REACH_DISTANCE.get()))), BlockMode.COLLIDER, FluidMode.NONE, player));
                if (result.getType() == RayTraceResult.Type.MISS) { //Allow player to shoot weapon if we are looking beyond our player reach distance and sneaking
                    player.setActiveHand(handIn);
                    return ActionResult.resultSuccess(gun);
                }
                else {
                    return ActionResult.resultFail(gun);
                }
            }
            else
                player.setActiveHand(handIn);
            return new ActionResult<>(ActionResultType.SUCCESS, gun);
        }
        return ActionResult.resultSuccess(gun); 
        
    }

    @Override
    public int getUseDuration(ItemStack stack) {
        return 72000;
    }

    @Override
    public void onPlayerStoppedUsing(ItemStack stack, World world, LivingEntity entityLiving, int timeLeft) {
        if (!world.isRemote()) {
            if (entityLiving instanceof PlayerEntity) {
                PlayerEntity player = (PlayerEntity) entityLiving;
                if (this.getNumShotsRemaining(stack) > 0 || player.abilities.isCreativeMode) {
                    LaserWeapon weapon = new LaserWeapon(player);
                    weapon.useWeapon(player);
                    weapon.setDamage(this.getCurrentTier(stack).getDamageInflicted());
                }
                else {
                    player.world.playSound(null, player.getPosition(), SoundEvents.BLOCK_DISPENSER_FAIL, SoundCategory.PLAYERS, 0.5F, 1F);
                }
            }
        }
    }

    @Override
    public boolean shouldCauseReequipAnimation(ItemStack oldStack, ItemStack newStack, boolean slotChanged) {
        return false;
    }

    @Override
    public void addInformation(ItemStack stack, World worldIn, List<ITextComponent> tooltip, ITooltipFlag flagIn) {
         TranslationTextComponent charge = new TranslationTextComponent("tooltip.laser_gun.charge");
        if (getTotalCharge(stack) > 0) {
            tooltip.add(charge.appendSibling(new StringTextComponent(String.valueOf(this.getTotalCharge(stack))).mergeStyle(TextFormatting.LIGHT_PURPLE)).appendSibling(TardisConstants.Suffix.ARTRON_UNITS));
        } else {
            tooltip.add(NONE.mergeStyle(TextFormatting.RED));
        }
        tooltip.add(new TranslationTextComponent("tooltip.laser_gun.shots_left").appendSibling(new StringTextComponent(String.valueOf(this.getNumShotsRemaining(stack))).mergeStyle(TextFormatting.LIGHT_PURPLE)));
        tooltip.add(TardisConstants.Translations.TOOLTIP_HOLD_SHIFT);
        tooltip.add(TardisConstants.Translations.TOOLTIP_CONTROL);
        if (Screen.hasShiftDown()) {
            tooltip.clear();
            tooltip.add(0, stack.getDisplayName());
            tooltip.add(new TranslationTextComponent("tooltip.laser_gun.num_batteries").appendSibling(new StringTextComponent(String.valueOf(this.getNumBatteries(stack))).mergeStyle(TextFormatting.LIGHT_PURPLE)));
            IFormattableTextComponent tier = new TranslationTextComponent(this.getCurrentTier(stack).getTranslationKey()).mergeStyle(TextFormatting.LIGHT_PURPLE);
            tooltip.add(new TranslationTextComponent("tooltip.laser_gun.tier").appendSibling(tier));
        }
        if (Screen.hasControlDown()) {
            tooltip.clear();
            tooltip.add(0, stack.getDisplayName());
            tooltip.add(TardisConstants.Translations.TOOLTIP_ARTRON_BATTERY_REQUIRED);
            TranslationTextComponent keyBind = new TranslationTextComponent(TClientRegistry.RELOAD_GUN.getTranslationKey());
            IFormattableTextComponent description = new TranslationTextComponent("tooltip.laser_gun.description", keyBind).mergeStyle(TextFormatting.GRAY);
            final IFormattableTextComponent finalDesc = TardisConstants.Prefix.TOOLTIP_ITEM_DESCRIPTION.deepCopy().appendSibling(description);
            tooltip.add(finalDesc);
        }
        super.addInformation(stack, worldIn, tooltip, flagIn);
    }

}
