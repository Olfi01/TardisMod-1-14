package net.tardis.mod.items;

import java.util.List;

import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.client.world.ClientWorld;
import net.minecraft.entity.Entity;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.item.ItemFrameEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.text.IFormattableTextComponent;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraft.world.World;
import net.tardis.mod.Tardis;
import net.tardis.mod.cap.Capabilities;
import net.tardis.mod.cap.items.IWatch;
import net.tardis.mod.client.ClientHelper;
import net.tardis.mod.helper.PlayerHelper;
import net.tardis.mod.helper.TextHelper;
import net.tardis.mod.itemgroups.TItemGroups;
import net.tardis.mod.items.misc.TooltipProviderItem;
import net.tardis.mod.properties.Prop;
import net.tardis.mod.sounds.TSounds;

public class PocketWatchItem extends TooltipProviderItem {
    
	private final IFormattableTextComponent descriptionTooltip = TextHelper.createDescriptionItemTooltip(new TranslationTextComponent("tooltip." + Tardis.MODID + ".watch.line1"));
	private final IFormattableTextComponent descriptionTooltipTwo = TextHelper.createExtraLineItemTooltip(new TranslationTextComponent("tooltip." + Tardis.MODID + ".watch.line2"));
	
	public PocketWatchItem() {
        super(Prop.Items.ONE.get().group(TItemGroups.MAIN));
        this.setShowTooltips(true);
        this.setHasDescriptionTooltips(true);
    }

    @Override
	public void createDescriptionTooltips(ItemStack stack, World worldIn, List<ITextComponent> tooltip,
	        ITooltipFlag flagIn) {
    	tooltip.add(descriptionTooltip);
        tooltip.add(descriptionTooltipTwo);
	}

	@Override
    public void inventoryTick(ItemStack stack, World worldIn, Entity entityIn, int itemSlot, boolean isSelected) {
        if (entityIn instanceof PlayerEntity || entityIn instanceof ItemFrameEntity) {
            if (PlayerHelper.isInEitherHand((LivingEntity) entityIn, stack.getItem()) || entityIn instanceof ItemFrameEntity) {
                if (worldIn.getGameTime() % 20 == 0) {
                    worldIn.playSound(null, entityIn.getPosition(), TSounds.WATCH_TICK.get(), SoundCategory.BLOCKS, 0.25F, 1F);
                }
                if (worldIn.getGameTime() % 100 == 0) {
                    stack.getCapability(Capabilities.WATCH_CAPABILITY).ifPresent(watch ->
                    {
                        watch.tick(worldIn, entityIn, itemSlot);
                        if (watch.shouldSpin(entityIn) && worldIn.getGameTime() % 10 == 0) {
                            worldIn.playSound(null, entityIn.getPosition(), TSounds.WATCH_MALFUNCTION.get(), SoundCategory.BLOCKS, 0.5F, 1F);
                        }
                    });
                }
            }
        }
    }
    
    
    public float createPropertyOverride(ItemStack stack, ClientWorld world, LivingEntity entity) {
        IWatch watch = stack.getCapability(Capabilities.WATCH_CAPABILITY).orElse(null);
        if (watch != null) {
            if (entity != null && watch.shouldSpin(entity))
                return entity.ticksExisted % 8;
            else return watch.getVariant();
        }
        return world != null ? ClientHelper.getPocketWatchRotFromTime(world) : 0;
    }
}
