package net.tardis.mod.items.sonicparts;

import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.util.text.TranslationTextComponent;
import net.tardis.mod.sonic.ISonicPart;

/**
 * Created by Swirtzly
 * on 21/03/2020 @ 21:11
 */
public class SonicBasePart extends Item implements ISonicPart {

    private SonicPart sonicPart;

    public SonicBasePart(SonicPart part) {
        this(part, new Properties().maxStackSize(1));
    }
    
    public SonicBasePart(SonicPart part, Properties prop) {
    	super(prop);
    	this.sonicPart = part;
    }

    public static void setType(ItemStack sonic, SonicComponentTypes type) {
        if (sonic.getTag() == null) {
            sonic.setTag(new CompoundNBT());
        }
        sonic.getTag().putInt("type", type.getId());
    }

    public static int getType(ItemStack sonic) {
        if (sonic.getTag() != null) {
            return sonic.getTag().getInt("type");
        }
        return SonicComponentTypes.MK_1.getId();
    }

    @Override
    public SonicPart getSonicPart() {
        return sonicPart;
    }

    @Override
    public void update() {

    }

    public enum SonicComponentTypes {
        MK_1(0), MK_2(1), MK_3(2), MK_4(3), MK_5(4), MK_6(5), MK_7(6);

        private int id;

        SonicComponentTypes(int id) {
            this.id = id;
        }

        public static SonicComponentTypes getFromID(int id) {
            for (SonicComponentTypes type : SonicComponentTypes.values()) {
                if (type.getId() == id)
                    return type;
            }
            return MK_1;
        }

        public int getId() {
            return id;
        }

        public TranslationTextComponent getName() {
            return new TranslationTextComponent("sonic.component_name." + name().toLowerCase());
        }
    }
}
