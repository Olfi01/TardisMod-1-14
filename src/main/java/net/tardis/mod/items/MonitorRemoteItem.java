package net.tardis.mod.items;

import java.util.List;

import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ActionResult;
import net.minecraft.util.ActionResultType;
import net.minecraft.util.Hand;
import net.minecraft.util.math.BlockRayTraceResult;
import net.minecraft.util.math.RayTraceContext;
import net.minecraft.util.math.RayTraceContext.BlockMode;
import net.minecraft.util.math.RayTraceContext.FluidMode;
import net.minecraft.util.math.RayTraceResult;
import net.minecraft.util.math.vector.Vector3d;
import net.minecraft.util.text.IFormattableTextComponent;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraft.world.World;
import net.tardis.mod.client.ClientHelper;
import net.tardis.mod.constants.TardisConstants;
import net.tardis.mod.contexts.gui.GuiContextBlock;
import net.tardis.mod.helper.TextHelper;
import net.tardis.mod.itemgroups.TItemGroups;
import net.tardis.mod.items.misc.TooltipProviderItem;
import net.tardis.mod.misc.IMonitor;
import net.tardis.mod.properties.Prop;

public class MonitorRemoteItem extends TooltipProviderItem {
	protected final IFormattableTextComponent descriptionTooltip = TextHelper.createDescriptionItemTooltip(new TranslationTextComponent("tooltip.monitor_remote.use"));
	
    public MonitorRemoteItem() {
        super(Prop.Items.ONE.get().group(TItemGroups.MAIN));
        this.setShowTooltips(true);
        this.setHasDescriptionTooltips(true);
    }

    @Override
    public ActionResult<ItemStack> onItemRightClick(World worldIn, PlayerEntity playerIn, Hand handIn) {
        if (worldIn.isRemote) {
            Vector3d pos = playerIn.getPositionVec().add(0, playerIn.getEyeHeight(), 0);
            RayTraceResult result = worldIn.rayTraceBlocks(new RayTraceContext(pos, pos.add(playerIn.getLookVec().scale(32)), BlockMode.COLLIDER, FluidMode.NONE, playerIn));
            if (result instanceof BlockRayTraceResult) {
                BlockRayTraceResult block = (BlockRayTraceResult) result;
                if (worldIn.getTileEntity(block.getPos()) instanceof IMonitor) {
                    ClientHelper.openGUI(TardisConstants.Gui.MONITOR_REMOTE, new GuiContextBlock(worldIn, block.getPos()));
                    return new ActionResult<ItemStack>(ActionResultType.SUCCESS, playerIn.getHeldItem(handIn));
                }
            }

        }

        return super.onItemRightClick(worldIn, playerIn, handIn);
    }

    @Override
    public void createDescriptionTooltips(ItemStack stack, World worldIn, List<ITextComponent> tooltip, ITooltipFlag flagIn) {
         tooltip.add(descriptionTooltip);
    }

}
