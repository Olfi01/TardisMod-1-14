package net.tardis.mod.commands.subcommands;

import java.util.Collections;

import com.mojang.brigadier.Command;
import com.mojang.brigadier.CommandDispatcher;
import com.mojang.brigadier.builder.ArgumentBuilder;
import com.mojang.brigadier.context.CommandContext;

import net.minecraft.command.CommandSource;
import net.minecraft.command.Commands;
import net.minecraft.command.ISuggestionProvider;
import net.minecraft.command.arguments.DimensionArgument;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.util.text.TextComponent;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraft.world.server.ServerWorld;
import net.tardis.mod.cap.Capabilities;
import net.tardis.mod.constants.TardisConstants;
import net.tardis.mod.helper.CommandHelper;
import net.tardis.mod.helper.TardisHelper;
import net.tardis.mod.helper.TextHelper;
import net.tardis.mod.items.misc.AttunableItem;
import net.tardis.mod.items.misc.IAttunable;
import net.tardis.mod.recipe.AttunableRecipe;

/** Attunes the player's currently held item if it's able to be attuned*/
public class AttuneItemCommand extends TCommand{
	
	private static AttunableRecipe selectedRecipe;
	
	private static int attuneHeldItemForCurrentTardis(CommandContext<CommandSource> context, ServerPlayerEntity player) {
		return attuneHeldItemForTardis(context, player, player.getServerWorld());
	}
	
	private static int attuneHeldItemForTardis(CommandContext<CommandSource> context, ServerPlayerEntity player, ServerWorld world){
		ItemStack heldStack = player.getHeldItemMainhand() == null || player.getHeldItemMainhand().isEmpty() ? player.getHeldItemOffhand() : player.getHeldItemMainhand();
		boolean isValid = false;
		for (AttunableRecipe recipe : AttunableRecipe.getAllRecipes(world)) {
			if (ItemStack.areItemsEqual(recipe.getInputIngredient().getMatchingStacks()[0], heldStack)) {
				isValid = true;
				selectedRecipe = recipe;
			}
		}
		if (heldStack != null && !heldStack.isEmpty() && (heldStack.getItem() instanceof IAttunable || isValid)) {
			if (!TardisHelper.getConsoleInWorld(world).isPresent()) {
				context.getSource().sendErrorMessage(new TranslationTextComponent(TardisConstants.Translations.NO_TARDIS_FOUND, world.getDimensionKey().getLocation().toString()));
				return 0;
			}
			else {
				TardisHelper.getConsoleInWorld(world).ifPresent(tile -> {
					tile.getWorld().getCapability(Capabilities.TARDIS_DATA).ifPresent(data -> {
						if (heldStack.getItem() instanceof IAttunable) {
							IAttunable attunable = (IAttunable)heldStack.getItem();
							attunable.onAttuned(heldStack, tile);
						}
						else {
							if (selectedRecipe != null) {
								ItemStack result = selectedRecipe.getRecipeOutput();
								if (heldStack.getCount() > result.getCount())
									result.setCount(heldStack.getCount());
								result = selectedRecipe.shouldAddNBTTags() ? AttunableItem.completeAttunement(result, tile) : result;
								int slot =  player.inventory.getSlotFor(heldStack);
								player.inventory.removeStackFromSlot(slot);
								player.inventory.add(slot, result);
							}
						}
					    TextComponent tardisIdentifier = TextHelper.getTardisDimObject(world, data);
					    context.getSource().sendFeedback(new TranslationTextComponent("command.tardis.attune.success", heldStack.getDisplayName().getString(), tardisIdentifier), true);
					});
				});
				return Command.SINGLE_SUCCESS;
			}
		}
		else {
			context.getSource().sendErrorMessage(new TranslationTextComponent("command.tardis.attune.invalid", heldStack.getDisplayName().getString()));
			return 0; //Error if something went wrong
		}
	}
	
	public static ArgumentBuilder<CommandSource, ?> register(CommandDispatcher<CommandSource> dispatcher){
	    return Commands.literal("attune_item").requires(context -> context.hasPermissionLevel(2))
		    .executes(context -> { return attuneHeldItemForCurrentTardis(context, context.getSource().asPlayer());})
		        .then(Commands.argument("tardis", DimensionArgument.getDimension()).suggests((context, suggestionBuilder) -> ISuggestionProvider.suggest(Collections.emptyList(), CommandHelper.addTardisKeysWithNameTooltip(suggestionBuilder, context.getSource().getServer())))
		            .executes(context -> attuneHeldItemForTardis(context, context.getSource().asPlayer(), DimensionArgument.getDimensionArgument(context, "tardis"))));
	}

}
