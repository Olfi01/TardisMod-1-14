package net.tardis.mod.upgrades;

import net.minecraft.item.Item;
import net.minecraft.util.RegistryKey;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraft.world.server.ServerWorld;
import net.tardis.mod.helper.TardisHelper;
import net.tardis.mod.items.TimeLinkUpgradeItem;
import net.tardis.mod.misc.SpaceTimeCoord;
import net.tardis.mod.subsystem.Subsystem;
import net.tardis.mod.tileentities.ConsoleTile;

import java.util.Optional;

public class TimeLinkUpgrade extends Upgrade{

	private Optional<ConsoleTile> slave = Optional.empty();
	
	public TimeLinkUpgrade(UpgradeEntry entry, ConsoleTile tile, Class<? extends Subsystem> clazz) {
		super(entry, tile, clazz);
	}

	@Override
	public void onLand() {
		

		if(this.isUsable() && this.isActivated()) {
			if(!slave.isPresent())
				findSlave();
			this.slave.ifPresent(tile -> {
				BlockPos pos;
				//If the parent Tardis has already preparing to land, use its current position
				//By this time, the current location will already be set, and this is where the exterior blocks are going to be placed.
            	if (this.getConsole().getLandTime() <= 0) {
            	    pos = this.getConsole().getCurrentLocation().offset(this.getConsole().getExteriorFacingDirection(), 3);
            	}
            	else {
                	pos = this.getConsole().getPositionInFlight().getPos().offset(this.getConsole().getExteriorFacingDirection(), 3);
                }
            	
				//Update the towed Tardis' destination but don't force it to land
            	//If we call initLand here, it'll land, but remove its exterior if its still in flight, then reland again
            	//During the second landing, the destination coordinates get scaled once again, so the towed Tardis may land tens of blocks away from the first landing spot.
				tile.setDestination(new SpaceTimeCoord(this.getConsole().getDestinationDimension(), pos));
				tile.setCurrentLocation(this.getConsole().getCurrentDimension(), pos);
				
			});
		}
		else {
			if(slave.isPresent())
		        this.unlink();
		}
		
	}

	@Override
	public void onTakeoff() {
		
		if(this.isUsable() && this.isActivated()) {
			if(!slave.isPresent())
				findSlave();

			this.slave.ifPresent(tile -> {
				tile.takeoffTowed();
			});
		}
		else {
			if(slave.isPresent())
		        this.unlink();
		}
	}

	@Override
	public void onFlightSecond() {
		if(this.isUsable() && this.isActivated()) {
			this.slave.ifPresent(tile -> {
				SpaceTimeCoord coord = new SpaceTimeCoord(this.getConsole().getDestinationDimension(), this.getConsole().getDestinationPosition().offset(this.getConsole().getExteriorFacingDirection(), 3));
				tile.setDestination(coord);
			});
		}
	}
	
	public Optional<ConsoleTile> getSlave() {
		return this.slave;
	}
	
	public void setSlave(Optional<ConsoleTile> tile) {
		this.slave = tile;
	}
	
	public void unlink() {
		this.slave = Optional.empty();
	}
	
	private void findSlave() {
		Item item = this.getStack().getItem();
		if (item != null && item instanceof TimeLinkUpgradeItem) {
			TimeLinkUpgradeItem link = (TimeLinkUpgradeItem)item;
			RegistryKey<World> type = link.getConsoleWorldKey(getStack());
			if(type != null) {
				ServerWorld world = this.getConsole().getWorld().getServer().getWorld(type);
				TardisHelper.getConsoleInWorld(world).ifPresent(slave -> this.setSlave(Optional.of(slave)));
			}
		}
		
	}


}
