package net.tardis.mod.cap.items;

import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.nbt.INBT;
import net.minecraft.util.Direction;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.capabilities.Capability.IStorage;
import net.minecraftforge.common.capabilities.ICapabilitySerializable;
import net.minecraftforge.common.util.INBTSerializable;
import net.minecraftforge.common.util.LazyOptional;
import net.minecraftforge.items.ItemStackHandler;
import net.tardis.mod.cap.Capabilities;

public interface IVortexCap extends INBTSerializable<CompoundNBT> {

    float getShieldHealth();

    void setShieldHealth(float health);

    void tick(PlayerEntity player);

    float getDischargeAmount();

    void setDischargeAmount(float amount);

    float getTotalCurrentCharge();

    ItemStackHandler getItemHandler();

    void setTeleported(boolean teleport);

    float getClientTotalCurrentCharge();

    int getBatteries();

    boolean getOpen();

    void setOpen(boolean open);

    boolean wasVMUsed();

    void setVmUsed(boolean type);
    /** 1.17: IStorage interface removed with no replacement. Move these methods to the provider that implements {@linkplain ICapabilitySerializable}*/
    @Deprecated
    public static class Storage implements IStorage<IVortexCap> {

        @Override
        public INBT writeNBT(Capability<IVortexCap> capability, IVortexCap instance, Direction side) {
            return instance.serializeNBT();
        }

        @Override
        public void readNBT(Capability<IVortexCap> capability, IVortexCap instance, Direction side, INBT nbt) {
            instance.deserializeNBT((CompoundNBT) nbt);
        }

    }

    public static class Provider implements ICapabilitySerializable<CompoundNBT> {

        private IVortexCap vortex;

        public Provider(IVortexCap vortex) {
            this.vortex = vortex;
        }

        public Provider(ItemStack stack) {
            this.vortex = new VortexCapability(stack);
        }

        @SuppressWarnings("unchecked")
        @Override
        public <T> LazyOptional<T> getCapability(Capability<T> cap, Direction side) {
            return cap == Capabilities.VORTEX_MANIP ? (LazyOptional<T>) LazyOptional.of(() -> (T) vortex) : LazyOptional.empty();
        }

        @Override
        public CompoundNBT serializeNBT() {
            return vortex.serializeNBT();
        }

        @Override
        public void deserializeNBT(CompoundNBT nbt) {
            vortex.deserializeNBT(nbt);
        }

    }
}
