package net.tardis.mod.sounds;

public class SoundSchemeMaster extends SoundSchemeBase {

	public SoundSchemeMaster() {
		super(() -> TSounds.MASTER_TAKEOFF.get(), () -> TSounds.MASTER_LAND.get(), () -> TSounds.TARDIS_FLY_LOOP.get());
	}
	
	@Override
	public int getLandTime() {
		return 260;
	}

	@Override
	public int getTakeoffTime() {
		return 360;
	}
	
}
