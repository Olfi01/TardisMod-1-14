package net.tardis.mod.tags;

import net.minecraft.block.Block;
import net.minecraft.tags.BlockTags;
import net.minecraft.tags.ITag;
import net.minecraft.util.ResourceLocation;
import net.tardis.mod.Tardis;
import net.tardis.mod.helper.Helper;

public class TardisBlockTags {

    public static final ITag<Block> BLOCKED = makeBlock(new ResourceLocation(Tardis.MODID, "tardis_proof"));
    public static final ITag<Block> CREW_TASK = makeBlock(Helper.createRL("crew_tasks"));
    public static final ITag<Block> RECLAMATION_BLACKLIST = makeBlock(Helper.createRL("reclamation_blacklist"));

    public static ITag.INamedTag<Block> makeBlock(ResourceLocation resourceLocation) {
        return BlockTags.createOptional(resourceLocation);
    }
}
