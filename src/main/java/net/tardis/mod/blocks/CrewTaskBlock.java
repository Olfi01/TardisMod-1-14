package net.tardis.mod.blocks;

import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.state.BooleanProperty;
import net.minecraft.state.StateContainer;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

public class CrewTaskBlock extends Block implements ICrewTask{

    public static final BooleanProperty IS_USED = BooleanProperty.create("crew_use");

    public CrewTaskBlock(Properties properties) {
        super(properties);
    }

    @Override
    protected void fillStateContainer(StateContainer.Builder<Block, BlockState> builder) {
        super.fillStateContainer(builder);
        builder.add(IS_USED);
    }

    @Override
    public void setUsing(boolean used, World world, BlockPos pos) {
        if(!world.isRemote)
            world.setBlockState(pos, world.getBlockState(pos).with(IS_USED, used));
    }

    @Override
    public boolean isBeingUsed(World world, BlockPos pos) {
        return world.getBlockState(pos).get(IS_USED);
    }
}
