package net.tardis.mod.blocks;

import net.minecraft.block.BlockState;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.inventory.InventoryHelper;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ActionResultType;
import net.minecraft.util.Hand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.BlockRayTraceResult;
import net.minecraft.world.World;
import net.tardis.mod.blocks.template.NotSolidTileBlock;
import net.tardis.mod.cap.Capabilities;
import net.tardis.mod.helper.TInventoryHelper;
import net.tardis.mod.properties.Prop;
import net.tardis.mod.tileentities.machines.ArtronCollectorTile;

public class ArtronCollectorBlock extends NotSolidTileBlock {

    public ArtronCollectorBlock() {
        super(Prop.Blocks.BASIC_TECH.get());
    }

    @Override
    public ActionResultType onBlockActivated(BlockState state, World worldIn, BlockPos pos, PlayerEntity player, Hand handIn, BlockRayTraceResult hit) {
        
        if(handIn == Hand.OFF_HAND)
            return ActionResultType.FAIL;
        
        ArtronCollectorTile tile = (ArtronCollectorTile)worldIn.getTileEntity(pos);
        ItemStack stack = player.getHeldItem(handIn);
            
        if(!tile.getItem().isEmpty())
            this.dropCurrentItem(tile, player);
        if (!stack.isEmpty()) { //Check if the player's held stack is not empty, then copy
            tile.placeItem(stack.copy());
            //Set the held stack to empty since we are setting an copy of the old stack
            //Don't shrink held item by one, otherwise we only shrink the held stack by one, but the ORIGINAL stack is being copied to the tile
            //If you then right click again, you will get the tile's stack AND your existing held stack, which allows for infinite item duplication
            //This new behaviour essentially swaps the tile's stack for your held stack
            player.setHeldItem(handIn, ItemStack.EMPTY); 
            tile.update();
        }
        return ActionResultType.SUCCESS;
    }
    
    @Override
    public void onReplaced(BlockState state, World worldIn, BlockPos pos, BlockState newState, boolean isMoving) {
        if(!worldIn.isRemote() && state.getBlock() != newState.getBlock()) {
            ArtronCollectorTile collect = (ArtronCollectorTile)worldIn.getTileEntity(pos);
            if(!collect.getItem().isEmpty())
                InventoryHelper.spawnItemStack(worldIn, pos.getX() + 0.5, pos.getY() + 1, pos.getZ() + 0.5, collect.getItem());
            worldIn.getChunkAt(pos).getCapability(Capabilities.RIFT).ifPresent(cap -> {
                if (cap.isRift()) {
                    cap.addEnergy(collect.getArtron()); //Release stored artron back into the rift chunk.
                }
            });
        }
        super.onReplaced(state, worldIn, pos, newState, isMoving);
    }

    /**
     * 
     * @param tile
     * @return - True if anything was spawned
     */
    public boolean dropCurrentItem(ArtronCollectorTile tile, PlayerEntity player) {
        if(!tile.getItem().isEmpty()) {
            TInventoryHelper.giveStackTo(player, tile.getItem());
            tile.placeItem(ItemStack.EMPTY);
            tile.update();
            return true;
        }
        return false;
    }

}
