package net.tardis.mod.blocks;

import java.util.List;

import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.client.gui.screen.Screen;
import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.BlockItemUseContext;
import net.minecraft.item.ItemStack;
import net.minecraft.state.BooleanProperty;
import net.minecraft.state.StateContainer;
import net.minecraft.util.ActionResultType;
import net.minecraft.util.Hand;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.BlockRayTraceResult;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.TextFormatting;
import net.minecraft.world.IBlockReader;
import net.minecraft.world.World;
import net.tardis.mod.ars.IARS;
import net.tardis.mod.blocks.template.NotSolidTileBlock;
import net.tardis.mod.constants.TardisConstants;
import net.tardis.mod.properties.Prop;
import net.tardis.mod.sounds.TSounds;
import net.tardis.mod.tileentities.AntiGravityTile;

/**
 * Created by Swirtzly
 * on 06/04/2020 @ 22:08
 */
public class AntiGravBlock extends NotSolidTileBlock implements IARS {

    public static final BooleanProperty ACTIVATED = BooleanProperty.create("activated");

    public AntiGravBlock() {
        super(Prop.Blocks.BASIC_TECH.get());
        this.setDefaultState(this.getDefaultState().with(ACTIVATED, Boolean.FALSE));
    }

    @Override
    public ActionResultType onBlockActivated(BlockState state, World worldIn, BlockPos pos, PlayerEntity player, Hand handIn, BlockRayTraceResult hit) {
        if (player.isSneaking()) {
            AntiGravityTile gravityTile = (AntiGravityTile) worldIn.getTileEntity(pos);
            if (gravityTile != null) {
                gravityTile.setRange(gravityTile.getRange() + 1);
            }
        }
        return ActionResultType.SUCCESS;
    }

    @Override
    public void neighborChanged(BlockState state, World worldIn, BlockPos pos, Block blockIn, BlockPos fromPos, boolean isMoving) {
        if (!worldIn.isRemote) {
            boolean powered = worldIn.isBlockPowered(pos);
            if (powered != state.get(ACTIVATED)) {
            	BlockState newState = state.with(ACTIVATED, Boolean.valueOf(powered));
                worldIn.playSound(null, pos, powered ? TSounds.SUBSYSTEMS_ON.get() : TSounds.SUBSYSTEMS_OFF.get(), SoundCategory.BLOCKS, 0.5F, 1F);
                worldIn.setBlockState(pos, newState, 2);
            }

        }
    }

    @Override
    protected void fillStateContainer(StateContainer.Builder<Block, BlockState> builder) {
        builder.add(ACTIVATED);
        super.fillStateContainer(builder);
    }


	@Override
	public BlockState getStateForPlacement(BlockItemUseContext context) {
		return this.getDefaultState().with(ACTIVATED, false);
	}

    @Override
    public void addInformation(ItemStack stack, IBlockReader worldIn, List<ITextComponent> tooltip, ITooltipFlag flagIn) {
        if (Screen.hasControlDown())
            tooltip.add(TardisConstants.Translations.TOOLTIP_REDSTONE_REQUIRED.mergeStyle(TextFormatting.GRAY));
        else
            tooltip.add(TardisConstants.Translations.TOOLTIP_CONTROL);
    }

}
