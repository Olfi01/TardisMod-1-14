package net.tardis.mod.datagen;

import java.io.IOException;
import java.util.List;

import com.google.common.collect.Lists;

import net.minecraft.block.ComposterBlock;
import net.minecraft.data.DataGenerator;
import net.minecraft.data.DirectoryCache;
import net.minecraft.data.IDataProvider;
import net.minecraft.util.IItemProvider;
import net.minecraft.util.ResourceLocation;
import net.tardis.mod.helper.Helper;

public class TardisPlantTag extends TardisItemTagGen{

	public TardisPlantTag(DataGenerator gen){
		super(gen);
	}
	
	@Override
	public void act(DirectoryCache cache) throws IOException {
		List<ResourceLocation> items = Lists.newArrayList();
		
		ComposterBlock.CHANCES.forEach((IItemProvider item, Float unused) -> {
			items.add(item.asItem().getRegistryName());
		});
		
		IDataProvider.save(DataGen.GSON, cache, this.serialize(items), getPath(this.generator.getOutputFolder(), Helper.createRL("plants")));
		
	}

	@Override
	public String getName() {
		return "TARDIS Plant Tag";
	}

}
