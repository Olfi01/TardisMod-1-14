package net.tardis.mixin;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.gen.Invoker;

import com.mojang.serialization.Codec;

import net.minecraft.world.gen.ChunkGenerator;

@Mixin(ChunkGenerator.class)
public interface ChunkGeneratorInvokerMixin {
	
	@Invoker("func_230347_a_")
	public Codec<? extends ChunkGenerator> codec();

}
